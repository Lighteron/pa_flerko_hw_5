# 1. Создать view-функцию, которая возвращает количество уникальных имен (FirstName) в таблице Customers.
# Вью-фукнция: def get_unique_names() -> int
# Url: /unique_names
# 2. Создать view-функцию, которая выводит количество записей из таблицы Tracks.
# Вью-фукнция: def get_tracks_count() -> int
# Url: /tracks_count
# 3. Создать view-функцию, которая ищет вхождения текста по всем текстовым полямтаблицы Customers
# (используйте оператор OR в sql-запросе). Параметр text опциональный, если не передан, то отбираются все записи.
# Вью-фукнция: def get_customers() -> [str]
# Url: /customers?text=jo
# 4. [необязательно] Вывести сумму всех продажи компании из таблицы Invoice_Items
# как сумму всех произведений (UnitPrice * Quantity)
# Вью-фукнция: def get_sales() -> float
# Url: /sales

from flask import Flask, jsonify
from db import execute_query
from faker import Faker
from utils import format_records

app = Flask(__name__)
fake = Faker()


@app.errorhandler(422)
@app.errorhandler(400)
def handle_error(err):
    headers = err.data.get("headers", None)
    messages = err.data.get("messages", ["Invalid request."])
    if headers:
        return jsonify({"errors": messages}), err.code, headers
    else:
        return jsonify({"errors": messages}), err.code


@app.route("/unique_names")
def get_unique_names():
    records = execute_query("SELECT DISTINCT count(FirstName) from customers")
    result = f'Number of unique names: {str(records[0][0])}'
    return result


@app.route("/tracks_count")
def get_tracks_count():
    records = execute_query("SELECT count(TrackId) from tracks")
    result = f'The number of records from the tracks table: {str(records[0][0])}'
    return result


@app.route('/fill_companies')
def fill_companies():
    query = 'SELECT customers.CustomerId FROM customers WHERE Company is NULL'
    empty_lines = execute_query(query)
    for item in empty_lines:
        company = Faker('EN').company()
        customerID = item[0]
        query = f'UPDATE customers set Company = "{company}" where CustomerID is {customerID}'
        execute_query(query)
    return 'Success'


app.run(debug=True)

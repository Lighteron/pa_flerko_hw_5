from webargs import fields


def format_records(records):
    return "<br>".join(str(rec) for rec in records)


params = {
        "text": fields.Str(
            required=False,
            missing=None,
        )
    }

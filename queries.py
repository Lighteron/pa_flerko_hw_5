from db import execute_query

CustomersNoParamQuery = 'SELECT * FROM customers '

GetFieldsQuery = "SELECT name FROM PRAGMA_TABLE_INFO( 'customers' )"


def add_params_to_customers_query(text: str):

    fields = execute_query(GetFieldsQuery)

    start = 'WHERE '
    query = []

    for field in fields:
        query.append(f"{field[0]} LIKE ('%{text}%')")

    return start + ' OR '.join(query)
